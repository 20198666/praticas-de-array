﻿using System;

namespace practica_array4
{
    class Program
    {
        static void Main(string[] args)
        {/*Un programa que almacene en una tabla el número de días que tiene cada mes (supondremos que es un año no bisiesto), 
         pida al usuario que le indique un mes (1=enero, 12=diciembre) y muestre en pantalla el número de días que tiene ese mes.*/

            int eli;
            int[] month = new int[12];
      
            month[0] = 31;
            month[1] = 28;
            month[2] = 31;
            month[3] = 30;
            month[4] = 31;
            month[5] = 30;
            month[6] = 31;
            month[7] = 31;
            month[8] = 30;
            month[9] = 30;
            month[10] = 30;
            month[11] = 31;

            Console.WriteLine("Elija el mes del cual sea conocer cuantos dia tiene" +"\n" +"1=enero, 2=febrero, 3=marzo, 4=abril, 5=mayo, 6=junio,"+"\n"+ "7=julio, 8=agosto, 9=septiembre, 10=octubre, 11=noviembre, 12=diciembre");
            eli = int.Parse(Console.ReadLine());
            
            switch (eli)
            {
                case 1:
                    Console.WriteLine("Enero tiene " + month[0] + " dias");
                    break;
                case 2:
                    Console.WriteLine("febrero tiene " + month[1] + " dias");
                    break;
                case 3:
                    Console.WriteLine("marzo tiene " + month[2] + " dias");
                    break;
                case 4:
                    Console.WriteLine("abril tiene " + month[3] + " dias");
                    break;
                case 5:
                    Console.WriteLine("mayo tiene " + month[4] + " dias");
                    break;
                case 6:
                    Console.WriteLine("junio tiene " + month[5] + " dias");
                    break;
                case 7:
                    Console.WriteLine("julio tiene " + month[6] + " dias");
                    break;
                case 8:
                    Console.WriteLine("agosto tiene " + month[7] + " dias");
                    break;
                case 9:
                    Console.WriteLine("septiembre tiene " + month[8] + " dias");
                    break;
                case 10:
                    Console.WriteLine("octubre tiene " + month[9] + " dias");
                    break;
                case 11:
                    Console.WriteLine("noviembre tiene " + month[10] + " dias");
                    break;
                case 12:
                    Console.WriteLine("diciembre tiene " + month[11] + " dias");
                    break;
            }
            Console.ReadKey();


            
        }
    }
}
