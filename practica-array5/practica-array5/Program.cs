﻿using System;

namespace practica_array5
{
    class Program
    {
        static void Main(string[] args)
        {/*Un programa que almacene en una tabla el número de días que tiene cada mes (de un año no bisiesto), pida al usuario que le indique un mes
          (ej. 2 para febrero) y un día (ej. el día 15) y diga qué número de día es dentro del año 
          (por ejemplo, el 15 de febrero sería el día número 46, el 31 de diciembre sería el día 365).*/

            int elija;
            int elijadia;
            int[] month =
            {
                31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
            };// 1   2   3   4   5   6   7   8   9  10  11  12

            Console.WriteLine("Elija el mes " + "\n" + "1=enero, 2=febrero, 3=marzo, 4=abril, 5=mayo, 6=junio," + "\n" + "7=julio, 8=agosto, 9=septiembre, 10=octubre, 11=noviembre, 12=diciembre");
            elija = int.Parse(Console.ReadLine());

            Console.WriteLine("Digite el dia");
            elijadia = int.Parse(Console.ReadLine());
            int suma;
            switch (elija)
            {
                case 1:
                    Console.WriteLine("Ha elijido Enero"+ month[0] );

                    if (elijadia <= 31)
                    {
                        Console.WriteLine("El dia seria el numero " + elijadia + " del año");
                    }
                    break;
                case 2:
                    Console.WriteLine("febrero  " + month[1] );
                    if (elijadia <=59)
                    {
                        suma = 31;
                        Console.WriteLine("El dia seria el numero " + (elijadia + suma) + " del año");
                    }
                    break;
                case 3:
                    Console.WriteLine("marzo  " + month[2] );
                    if (elijadia <=90)
                    {
                        suma = 59;
                        Console.WriteLine("El dia seria el numero " + (elijadia + suma) + " del año");
                    }
                    break;
                case 4:
                    Console.WriteLine("abril  " + month[3] );
                    if (elijadia <=120)
                    {
                        suma = 90;
                        Console.WriteLine("El dia seria el numero " + (elijadia + suma) + " del año");
                    }
                    break;
                case 5:
                    Console.WriteLine("mayo " + month[4] );
                    if (elijadia <=151)
                    {
                        suma = 120;
                        Console.WriteLine("El dia seria el numero " + (elijadia + suma) + "del año");
                    }
                    break;
                case 6:
                    Console.WriteLine("junio " + month[5] );
                    if (elijadia <=181)
                    {
                        suma = 151;
                        Console.WriteLine("El dia seria el numero " + (elijadia + suma) + "del año");
                    }
                    break;
                case 7:
                    Console.WriteLine("julio  " + month[6] );
                    if (elijadia <=212)
                    {
                        suma = 181;
                        Console.WriteLine("El dia seria el numero " + (elijadia + suma) + "del año");
                    }
                    break;
                case 8:
                    Console.WriteLine("agosto  " + month[7] );
                    if (elijadia <= 243) 
                    {
                        suma = 212;
                        Console.WriteLine("El dia seria el numero " + (elijadia + suma) + " del año");
                    }
                    break;
                case 9:
                    Console.WriteLine("septiembre  " + month[8] );
                    if (elijadia <= 273)
                    {
                        suma = 243;
                        Console.WriteLine("El dia seria el numero " + (elijadia + suma) + " del año");
                    }
                    break;
                case 10:
                    Console.WriteLine("octubre  " + month[9] );
                    if (elijadia <= 304)
                    {
                        suma = 273;
                        Console.WriteLine("El dia seria el numero " + (elijadia + suma) + " del año");
                    }
                    break;
                case 11:
                    Console.WriteLine("noviembre  " + month[10] );
                    if (elijadia <= 334)
                    {
                        suma = 304;
                        Console.WriteLine("El dia seria el numero " + (elijadia + suma) + " del año");
                    }
                    break;
                case 12:
                    Console.WriteLine("diciembre  " + month[11] );
                    if (elijadia <= 365)
                    {
                        suma = 334;
                        Console.WriteLine("El dia seria el numero " + (elijadia + suma) + " del año");
                    }
                    break;
            }
            Console.ReadKey();
        }
    }
}
