﻿using System;

namespace practica_array7
{
    class Program
    {
        static void Main(string[] args)
        {
            //Un programa que pida al usuario 10 números, calcule su media y luego muestre los que están por encima de la media.

            int[] number = new int[10];
            

            Console.WriteLine("Digete un numero");
            number[0] = int.Parse(Console.ReadLine());

            Console.WriteLine("Digete un numero");
            number[1] = int.Parse(Console.ReadLine());

            Console.WriteLine("Digete un numero");
            number[2] = int.Parse(Console.ReadLine());

            Console.WriteLine("Digete un numero");
            number[3] = int.Parse(Console.ReadLine());

            Console.WriteLine("Digete un numero");
            number[4] = int.Parse(Console.ReadLine());

            Console.WriteLine("Digete un numero");
            number[5] = int.Parse(Console.ReadLine());

            Console.WriteLine("Digete un numero");
            number[6] = int.Parse(Console.ReadLine());

            Console.WriteLine("Digete un numero");
            number[7] = int.Parse(Console.ReadLine());

            Console.WriteLine("Digete un numero");
            number[8] = int.Parse(Console.ReadLine());

            Console.WriteLine("Digete un numero");
            number[9] = int.Parse(Console.ReadLine());

            double suma = 0;
            
            for (int a = 0; a <= 9; a++ )
            {
                suma += number[a];
            }
            suma = suma / 10;
            Console.WriteLine("La media del numero es " + suma);

            for (int b = 0; b < number.Length; b++)
            {
                if (number[b] > suma)
                {
                    Console.WriteLine(number[b] + " Supera la media");
                }

            }
            Console.ReadKey();


        }
    }
}
