﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace practica_arrays
{
    class Program
    {
        static void Main(string[] args)
        { /*1. Un programa que pida al usuario 4 números, los memorice (utilizando una tabla), calcule su media aritmética y
            después muestre en pantalla la media y los datos tecleados.*/

            Console.WriteLine("programa para calcular la media aritmetica de 4 numeros");

            int[] number = new int[4];
            double suma, division = 0;
            int cont;
            
            Console.WriteLine("Digete un numero ");
            number[0] = int.Parse(Console.ReadLine());
            Console.WriteLine("Digete un numero ");
            number[1] = int.Parse(Console.ReadLine());
            Console.WriteLine("Digete un numero ");
            number[2] = int.Parse(Console.ReadLine());
            Console.WriteLine("Digete un numero ");
            number[3] = int.Parse(Console.ReadLine());


            suma = 0;
            for (cont = 0; cont <=3; cont++ ) 
            {
                suma = suma + number[cont];
                division = suma / 4;
            }
           
            Console.WriteLine("La media es: " + division);
            Console.WriteLine("Los valores que has usado son: "); //falta que muestre en pantalla los valores tecleados
            Console.ReadKey();
        }
    }
}
